FROM rust:slim

RUN apt-get update && apt-get install -yy pkg-config libssl-dev make
RUN rm -rf /var/cache/apt

RUN cargo install cargo-web
RUN rustup target add wasm32-unknown-unknown

WORKDIR /root
RUN mkdir bnw

WORKDIR /root/bnw/
COPY etc/start.sh /etc/start.sh
RUN chmod +x /etc/start.sh

ENTRYPOINT [ "/bin/bash", "-c", "/etc/start.sh" ]
