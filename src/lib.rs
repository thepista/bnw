#![recursion_limit = "256"]

mod image;

use stdweb::{js, web::FileList, Array, Value};
use yew::{
    ComponentLink, Component, ShouldRender, Html, html, ChangeData,
    services::reader::{File, ReaderTask, FileData, ReaderService},
};
use yew::agent::Packed;
use yew::services::ConsoleService;
use std::collections::hash_map::VacantEntry;


pub struct Model {
    link: ComponentLink<Self>,
    tasks: Vec<ReaderTask>,
    reader: ReaderService,

    console: ConsoleService
}

pub enum Msg {
    Image(Vec<u8>),
    Loaded(FileData),
    Ignore
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Model { link, tasks: Vec::new(), reader: ReaderService::new(), console: ConsoleService::new() }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Image(image_data) => {
                self.console.log(&format!("{:?}", image_data));
                true
            },
            Msg::Loaded(file) => {

                true
            },
            Msg::Ignore => false
        }
    }

    fn view(&self) -> Html {
        html! {
            <div>
                <div>
                    <input type="file" multiple=false onchange=self.link.callback(move |files| {
                        if let ChangeData::Files(file_list) = files {
                            Msg::Image(Self::get_image_data(file_list))
                        } else {
                            Msg::Ignore
                        }
                    })/>
                </div>
            </div>
        }
    }
}

impl Model {
    fn get_image_data(file_list: FileList) -> Vec<u8> {
        let value: Value = js! {
            var reader = new FileReader();
            reader.readAsDataURL(@{file_list}[0]);
            reader.onload = function(e){
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    var canvas = document.createElement("canvas");
                    var ctx = canvas.getContext("2d");

                    canvas.width = img.width;
                    canvas.height = img.height;
                    ctx.drawImage(img,0,0,img.width,img.height);

                    var imgData = ctx.getImageData(0, 0, img.width, img.height);

                    return imgData.data;
                }
            };
        };

        vec![1, 2, 3]
    }
}
