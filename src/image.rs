pub struct Image {
    data: Vec<u8>,
}

impl Image {
    pub fn new(image_data: Vec<u8>) -> Image {
        Image { data: image_data }
    }
}
