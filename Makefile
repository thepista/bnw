bs: bootstrap

bootstrap:
	@docker build . -t bnw

dev:
	@docker run -it --rm -v $(shell pwd):/root/bnw/ -p 8080:8080 --name bnw-web-server bnw || exit 0
